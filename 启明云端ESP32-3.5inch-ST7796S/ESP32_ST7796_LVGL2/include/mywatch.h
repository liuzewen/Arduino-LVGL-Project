#ifndef _MY_WATCH_H
#define _MY_WATCH_H

//固件版本号
#define Firmware_version "V 1.0.0"

//是否开启debug功能
const unsigned long BAUD_RATE = 115200;   //波特率

#define DEBUG 

#ifdef DEBUG 
#define DebugPrintln(message) Serial.println(message)
#else 
#define DebugPrintln(message)
#endif

#ifdef DEBUG 
#define DebugPrint(message) Serial.print(message) 
#else 
#define DebugPrint(message)
#endif

//星座信息

//默认WIFI信息
char ssid[] = "liuzewen";
char pswd[] = "17609245102liu";

//-----------------------GPIO配置-----------------------------
//LED配置
#define LED         2
#define LED_OFF   digitalWrite(LED, HIGH)//关灯
#define LED_ON    digitalWrite(LED, LOW)//开灯
#define LED_PWM   digitalWrite(LED, !digitalRead(LED))//灯闪烁
//蜂鸣器配置
#define BEEP        25
#define BEEP_ON   digitalWrite(BEEP, HIGH)//蜂鸣器响
#define BEEP_OFF  digitalWrite(BEEP, LOW)//蜂鸣器停
#define BEEP_PWM  digitalWrite(BEEP, !digitalRead(BEEP))//警报
//振动马达配置
#define MOTTER      26
#define MOTTER_ON   digitalWrite(MOTTER, HIGH)//开振动
#define MOTTER_OFF  digitalWrite(MOTTER, LOW)//振动停
#define MOTTER_PWM  digitalWrite(MOTTER, !digitalRead(MOTTER))//来回振动
//按键配置
#define UP_KEY      39
#define M_KEY       0
#define DOWN_KEY    34
//读取键值
#define KEYU  digitalRead(UP_KEY)
#define KEYM  digitalRead(M_KEY)
#define KEYD  digitalRead(DOWN_KEY)

#define NO_KEY_PRES     0
#define UP_KEY_PRES     1
#define M_KEY_PRES      2
#define DOWN_KEY_PRES   3

#define UP_KEY_LONG_PRES     11
#define M_KEY_LONG_PRES      22
#define DOWN_KEY_LONG_PRES   33

//触摸引脚
#define I2C_SDA 18
#define I2C_SCL 19
#define RST_N_PIN -1
#define INT_N_PIN 39

//显示屏驱动
#define TFT_CS        15
#define TFT_RST        22 // Or set to -1 and connect to Arduino RESET pin
#define TFT_DC         21
#define TFT_MOSI        13  // Data out
#define TFT_SCLK        14  // Clock out

#define TFT_BL  23  // Display backlight control pin

long up_pres_time = 0;
long m_pres_time = 0;
long down_pres_time = 0;

//wifi结构体
struct config_type{   
  char stassid[32];   
  char stapsw[64]; 
}config;

uint16_t Battery_capacity = 500;                         //电池电量

long key_long_pres_time = 1.2*1000;   //按键长时间按下时间

#endif /* _MY_WATCH_H */