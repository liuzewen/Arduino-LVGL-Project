#include <Arduino.h>

#define MOTTER1 25
#define MOTTER2 26
#define MOTTER3 36
#define MOTTER4 39

void setup()
{
    pinMode(MOTTER1,OUTPUT);
    pinMode(MOTTER2,OUTPUT);
    pinMode(MOTTER3,OUTPUT);
    pinMode(MOTTER4,OUTPUT);
    digitalWrite(MOTTER1,HIGH);
    digitalWrite(MOTTER2,LOW);
    digitalWrite(MOTTER3,LOW);
    digitalWrite(MOTTER4,HIGH);
}

void loop()
{
    delay(1000);
}