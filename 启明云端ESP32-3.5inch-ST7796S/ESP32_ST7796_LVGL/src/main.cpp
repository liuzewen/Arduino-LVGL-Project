#include "lcd.h"
#include "gui.h"
#include "test.h"
#include <lvgl.h>
#include "mywatch.h"
#include "FT6336U.h"
#include "mygui.h"
#include "demo.h"

lv_obj_t * slider_label;
int screenWidth = 320;
int screenHeight = 480;
int disp_cotter = 0;

static lv_disp_buf_t disp_buf;
static lv_color_t buf[LV_HOR_RES_MAX * 10];

void printEvent(String Event, lv_event_t event)
{
  
  Serial.print(Event);
  Serial.printf(" ");

  switch(event) {
      case LV_EVENT_PRESSED:
          Serial.printf("Pressed\n");
          break;

      case LV_EVENT_SHORT_CLICKED:
          Serial.printf("Short clicked\n");
          break;

      case LV_EVENT_CLICKED:
          Serial.printf("Clicked\n");
          break;

      case LV_EVENT_LONG_PRESSED:
          Serial.printf("Long press\n");
          break;

      case LV_EVENT_LONG_PRESSED_REPEAT:
          Serial.printf("Long press repeat\n");
          break;

      case LV_EVENT_RELEASED:
          Serial.printf("Released\n");
          break;
  }
}

/* Display flushing */
void my_disp_flush(lv_disp_drv_t *disp, const lv_area_t *area, lv_color_t *color_p)
{
	LCD_Color_Fill(area->x1,area->y1,area->x2,area->y2,(u16*)color_p);
    lv_disp_flush_ready(disp);
    //Serial.printf("屏幕刷新中:%d\n",disp_cotter++);
}

/* Reading input device (simulated encoder here) */
bool my_touchpad_read(lv_indev_drv_t * indev_driver, lv_indev_data_t * data)
{
    
	int16_t touchX, touchY;
	touchX = tp.tp[0].x;
	touchY = tp.tp[0].y;
	//bool touched = tp.tp[0].status;  //  bool TFT_eTouch<T>::getXY(int16_t& x, int16_t& y)
	bool touched = tp.touch_count;
    
    if(!touched)
    {
      return false;
    }

    if(touchX>screenWidth || touchY > screenHeight)
    {
      Serial.println("Y or y outside of expected parameters..");
      Serial.print("y:");
      Serial.print(touchX);
      Serial.print(" x:");
      Serial.print(touchY);
    }
    else
    {
      data->state = touched ? LV_INDEV_STATE_PR : LV_INDEV_STATE_REL; 
  
      /*Save the state and save the pressed coordinate*/
      //if(data->state == LV_INDEV_STATE_PR) touchpad_get_xy(&last_x, &last_y);
     
      /*Set the coordinates (if released use the last pressed coordinates)*/
      data->point.x = touchX;
      data->point.y = touchY;
  
      Serial.print("Data x");
      Serial.println(touchX);
      
      Serial.print("Data y");
      Serial.println(touchY);

    }

    return false; /*Return `false` because we are not buffering and no more data to read*/
}

void setup()
{
    Serial.begin(115200); /* prepare for possible serial debug */

    lv_init();
    //LCD初始化
	LCD_Init();	   //液晶屏初始化
    if (TFT_BL > 0) { // TFT_BL has been set in the TFT_eSPI library in the User Setup file TTGO_T_Display.h
		pinMode(TFT_BL, OUTPUT); // Set backlight pin to output mode
		digitalWrite(TFT_BL, HIGH); // Turn backlight on. TFT_BACKLIGHT_ON has been set in the TFT_eSPI library in the User Setup file TTGO_T_Display.h
    }

    //触摸屏初始化
    ft6336u.begin();
    Serial.print("FT6336U Firmware Version: "); 
    Serial.println(ft6336u.read_firmware_id());  
    Serial.print("FT6336U Device Mode: "); 
    Serial.println(ft6336u.read_device_mode());  

    lv_disp_buf_init(&disp_buf, buf, NULL, LV_HOR_RES_MAX * 10);

    /*Initialize the display*/
    lv_disp_drv_t disp_drv;
    lv_disp_drv_init(&disp_drv);
    disp_drv.hor_res = screenWidth;
    disp_drv.ver_res = screenHeight;
    disp_drv.flush_cb = my_disp_flush;
    disp_drv.buffer = &disp_buf;
    lv_disp_drv_register(&disp_drv);

    /*Initialize the (dummy) input device driver*/
	lv_indev_drv_t indev_drv;
	lv_indev_drv_init(&indev_drv);             /*Descriptor of a input device driver*/
	indev_drv.type = LV_INDEV_TYPE_POINTER;    /*Touch pad is a pointer-like device*/
	indev_drv.read_cb = my_touchpad_read;      /*Set your driver function*/
	lv_indev_drv_register(&indev_drv);         /*Finally register the driver*/

    //lv_ex_keyboard_1();
    //lv_ex_roller_1();
    //lv_ex_checkbox_1();
    //lv_ex_cpicker_1();
    //lv_ex_dropdown_2();//下拉框
    //lv_ex_gauge_1();//仪表盘
    lv_demo_widgets();
}


void loop()
{
	tp = ft6336u.scan();
    lv_task_handler(); /* let the GUI do its work */
    delay(5);
}
