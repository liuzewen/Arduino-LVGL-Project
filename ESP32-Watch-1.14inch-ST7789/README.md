# 目录说明
## 1.ESP32_LVGL_V6.1.2目录
本工程为 LVGL_V6.1.2 版本；
## 3.ESP32_LVGL_V7.1.x目录
本工程为TFT_eSPI-master库，LVGL_V7.1.x版本，并且驱动部分已经做了文件夹封装，文件目录更加清晰，其中main.cpp文件内容为：
```cpp
//UI
#include "my_gui\mygui.h"
#include "demo\demo.h"
#include "demo\demo1.h"
#include "demo\demo2.h"
#include "demo\demo3.h"
//驱动
#include "dev_port\dsp_dev.h"
#include "dev_port\fs_dev.h"
#include "dev_port\in_dev.h"
//udp
#include "udp_rtc.h"

void setup() {
  Serial.begin(115200);

  //link_wifi();//连接WIFI
  //udp_int();//初始化UDP

  //LVGL初始化
  lv_init();
  //屏幕驱动
  lv_port_disp_init();
  //输入驱动
  lv_port_indev_init();

  //lv_ex_tabview_1();//自己写的demo
  
  lv_demo_widgets();//官方demo
  //lv_demo_stress();//官方demo1
  //lv_ex_tileview_1();//官方demo2
  //lv_ex_get_started_2();//官方demo3
}

long rtc_time = 0;

void loop() {
  //if(millis()-rtc_time>1000)
  {
    //digitalClockDisplay();
  }
  lv_task_handler(); /* let the GUI do its work */
  delay(5);
}

```
其中，你可以选择各种demo来看下运行效果；
也可以修改代码为：
```cpp
//UI
#include "my_gui\mygui.h"
#include "demo\demo.h"
#include "demo\demo1.h"
#include "demo\demo2.h"
#include "demo\demo3.h"
//驱动
#include "dev_port\dsp_dev.h"
#include "dev_port\fs_dev.h"
#include "dev_port\in_dev.h"
//udp
#include "udp_rtc.h"

void setup() {
  Serial.begin(115200);

  link_wifi();//连接WIFI
  udp_int();//初始化UDP

  //LVGL初始化
  lv_init();
  //屏幕驱动
  lv_port_disp_init();
  //输入驱动
  lv_port_indev_init();

  lv_ex_tabview_1();//自己写的demo
  
  //lv_demo_widgets();//官方demo
  //lv_demo_stress();//官方demo1
  //lv_ex_tileview_1();//官方demo2
  //lv_ex_get_started_2();//官方demo3
}

long rtc_time = 0;

void loop() {
  if(millis()-rtc_time>1000)
  {
    digitalClockDisplay();
  }
  lv_task_handler(); /* let the GUI do its work */
  delay(5);
}
```
并在```udp_rtc.h```文件中修改WIFI为你自己的，即可看到我写的test界面；